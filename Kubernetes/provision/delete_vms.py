import boto3
import settings

session = boto3.Session(
	aws_access_key_id =settings.access_id,
	aws_secret_access_key = settings.access_key,
	)

ec2 = session.resource("ec2",region_name="ap-south-1")
client = session.client("ec2",region_name="ap-south-1")
waiter = client.get_waiter("instance_terminated")

ids = []

with open("kubernetes_ha.txt") as setups:
	for _setup in setups:
		_setup.replace("\n","")
		setup = _setup.split("|")
		for instance in ec2.instances.all():
			if(instance.private_ip_address) == setup[0]:
				ids.append(instance.instance_id)
print ids
ec2.instances.filter(InstanceIds = ids).terminate()
waiter.wait(InstanceIds=ids)