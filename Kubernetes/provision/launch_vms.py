import boto3
import settings

session = boto3.Session(
	aws_access_key_id =settings.access_id,
	aws_secret_access_key = settings.access_key,
	)

ec2 = session.resource("ec2",region_name=settings.region)
client = session.client("ec2",region_name=settings.region)

with open("kubernetes_ha.txt") as setups:
	for _setup in setups:
		_setup.replace("\n","")
		setup = _setup.split("|")
		instance = ec2.create_instances(
				ImageId = settings.ami_id,
				SecurityGroupIds=settings.security_groups,
				PrivateIpAddress=setup[0],
				KeyName=settings.key_name,
				MinCount=1, MaxCount=1,
				DisableApiTermination=False,
				BlockDeviceMappings=[{"DeviceName": settings.device,"Ebs" : { "VolumeSize" : 50 }}],
				Placement={
					'AvailabilityZone' : settings.avail_zone,
				},
				SubnetId=settings.subnet_id, InstanceType=setup[1],
				TagSpecifications=[
				{
					"ResourceType" : "instance",
					"Tags" : [
					{"Key": "Name", "Value": setup[2]},
					{"Key": "Setup", "Value": setup[3]},
					{"Key": "kubernetes.io/cluster/DemoK8s", "Value": "DemoK8s"}
					]
				}
				]
			)
waiter = client.get_waiter("instance_running")

response = instance[0].describe_attribute(
	DryRun=False,
	Attribute='instanceType'
	)
waiter.wait(
	DryRun=False,
	InstanceIds=[
		response["InstanceId"],
	],
	)