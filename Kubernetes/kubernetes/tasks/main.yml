---
# tasks file for kubernetes

- block:

  - name: set hostnames
    hostname:
      name: "{{ host_name }}"

  - name: add host_name to /etc/hosts
    lineinfile:
      dest: /etc/hosts
      regexp: '^127\.0\.0\.1[ \t]+localhost'
      line: '127.0.0.1 localhost {{ host_name }}'
      state: present
      
  - name: Add kubernetes repo
    copy:
      src: "kubernetes.repo"
      dest: /etc/yum.repos.d/kubernetes.repo

  - name: modprobe overlay
    shell: modprobe overlay

  - name: load br_netfilter
    shell: modprobe br_netfilter

  - name: Disable SELinux
    selinux:
      state: disabled

  - name: Setup required sysctl params, these persist across reboots.
    copy:
      src: "k8s.conf"
      dest: /etc/sysctl.d/k8s.conf

  - name: sysctl --system
    shell: sysctl --system

  - name: disable swap for now
    shell: swapoff -a

  - name: ensure swap is off after reboot
    replace:
      path: /etc/fstab
      regexp: '^(UUID.*swap.*)$'
      replace: '# \1'

  - name: Install kubelet, kubeadm, kubectl 
    shell: yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes

  - name: Start kubelet
    service:
      name: kubelet
      state: started
      enabled: yes

  - name: copy kubeadm-config.yaml
    copy:
      src: "kubeadm-config.yaml"
      dest: /tmp/kubeadm-config.yaml

  - name: Kubeadm init
    shell: "kubeadm init --config=/tmp/kubeadm-config.yaml --upload-certs > /tmp/kubernetes-join.txt"
    when: "'kubernetes-masters' in group_names and ansible_default_ipv4.address == hostvars[inventory_hostname]['groups']['kubernetes-masters'][0]"

  - name: "Create .kube directory"
    file:
      path: "/home/centos/.kube"
      state: directory
      mode: '0755'
    when: "'kubernetes-masters' in group_names and ansible_default_ipv4.address == hostvars[inventory_hostname]['groups']['kubernetes-masters'][0]"


  - name: Copy /etc/kubernetes/admin.conf to $HOME/.kube/config
    copy:
      src: /etc/kubernetes/admin.conf
      dest: /home/centos/.kube/config
      remote_src: yes
    when: "'kubernetes-masters' in group_names and ansible_default_ipv4.address == hostvars[inventory_hostname]['groups']['kubernetes-masters'][0]"

  - name: Configure CNI
    shell: "kubectl apply -f https://docs.projectcalico.org/v3.7/manifests/calico.yaml"
    become: yes
    become_user: centos
    when: "'kubernetes-masters' in group_names and ansible_default_ipv4.address == hostvars[inventory_hostname]['groups']['kubernetes-masters'][0]"

  - name: Get control node join command
    shell: 'less /tmp/kubernetes-join.txt | grep -i -B2 "\-\-control-plane"'
    register: control_join_command
    when: "'kubernetes-masters' in group_names and ansible_default_ipv4.address == hostvars[inventory_hostname]['groups']['kubernetes-masters'][0]"
    run_once: yes

  - name: Get worker node join command
    shell: "awk '/kubeadm join/{i++}i==2' /tmp/kubernetes-join.txt"
    register: worker_join_command
    when: "'kubernetes-masters' in group_names and ansible_default_ipv4.address == hostvars[inventory_hostname]['groups']['kubernetes-masters'][0]"
    run_once: yes

  # - debug: msg="control_join_command={{ control_join_command.stdout | regex_replace('\\\\\n','') }}"
  # #   # when: "'kubernetes-masters' in group_names and ansible_default_ipv4.address == hostvars[inventory_hostname]['groups']['kubernetes-masters'][0]"  

  # - debug: msg="worker_join_command={{ worker_join_command.stdout | regex_replace('\\\\\n','') }}"
  # #   # when: "'kubernetes-masters' in group_names and ansible_default_ipv4.address == hostvars[inventory_hostname]['groups']['kubernetes-masters'][0]"

  # - name: join secondary kubernetes-masters
  #   shell: "{{ control_join_command.stdout | regex_replace('\\\\\n','') }}"
  #   when: "'kubernetes-masters' in group_names and ansible_default_ipv4.address != hostvars[inventory_hostname]['groups']['kubernetes-masters'][0]"

  - name: join secondary kubernetes-masters
    shell: "{{ control_join_command.stdout | regex_replace('\\\\\n','') }}"
    delegate_to: "{{ item }}"
    with_items: "{{ groups['kubernetes-masters'][1:3] }}"
    run_once: true

  - name: join secondary kubernetes-workers
    shell: "{{ worker_join_command.stdout | regex_replace('\\\\\n','') }}"
    when: "'kubernetes-workers' in group_names"

  - name: Label worker nodes
    shell: "kubectl label node {{ hostvars[item]['host_name'] }} node-role.kubernetes.io/worker=worker"
    become: yes
    become_user: centos
    with_items: "{{ groups['kubernetes-workers'] }}"
    when: "'kubernetes-masters' in group_names and ansible_default_ipv4.address == hostvars[inventory_hostname]['groups']['kubernetes-masters'][0]"

  # - name: Flush IP table
  #   shell: "iptables -F && iptables -t nat -F && iptables -t mangle -F && iptables -X"

  - name: Install Ingress
    shell: "kubectl create -f https://raw.githubusercontent.com/jcmoraisjr/haproxy-ingress/master/docs/haproxy-ingress.yaml"
    become: yes
    become_user: centos
    when: "'kubernetes-masters' in group_names and ansible_default_ipv4.address == hostvars[inventory_hostname]['groups']['kubernetes-masters'][0]"

  - name: Label Ingress node
    shell: "kubectl label node {{ hostvars[item]['host_name'] }} role=ingress-controller"
    become: yes
    become_user: centos
    with_items: "{{ groups['kubernetes-workers'][0] }}"
    when: "'kubernetes-masters' in group_names and ansible_default_ipv4.address == hostvars[inventory_hostname]['groups']['kubernetes-masters'][0]"

  - name: Kube DNS
    shell: "kubectl delete pod -n kube-system  -l k8s-app=kube-dns"
    become: yes
    become_user: centos
    when: "'kubernetes-masters' in group_names and ansible_default_ipv4.address == hostvars[inventory_hostname]['groups']['kubernetes-masters'][0]"
  become: yes