# Kubernetes

# First try to launch a Highly Available Kubernetes cluster via Kubeadm.

### 1. This repo contains source code to
	- Provision AWS resources
	- Deploy Kubernetes HA with containerd as runtime
	- A LoadBalancer in the DMZ zone

    PS: This currently supports CentOS only. Can be extended further to other distributions and cloud environments.


### 2. The deployment architechture is
	- The cluster has 3 masters, 1 loadbalancer for control plane, 2 workers, 1 CI server with helm installed and 1 loadbalancer for external traffic.
	- For security reasons Kubernetes installation is deployed in the app subnet
	- A LoadBalancer(HAProxy) in the web subnet routes traffic to the Kubernetes cluster
	- Only port 80, 443, 1936 and SSH(for Bastion Host IP) are allowed in security groups of the App subnet
	- All Traffic between App subnet is allowed for this demo. 
	- For production usage refer to https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#check-required-ports for fine grained control.


### 3. Pre-requisites
	- A VPC with public and private subnets
	- A bastion host in the web subnet with Ansible installed(pip install ansible)
	- NAT gateway attached to route table associated with private subnet
	- Ports 80, 443 and 1936 allowed for security group for web subnet instances
	- A Bastion host in web subnet. Port 22 with source as the bastion host IP allowed in the web security group
	- Ports 80, 443 and 1936 allowed from the web CIDR in the app security group
	- Port 22 from source as the bastion host IP allowed in the app security group
	- All traffic allowed between the app subnet
	- AWS access id and secret key

# Execution
### 1) Clone the repo on the bastion host or ansible master of choice
	i) cd into "kubernetes/Kubernetes/provision" directory
	ii) edit the kubernetes_ha.txt file and enter the following:-
			XXX.xx.xx.x|t3.medium|Kubernetes HA Master1|Kubernentes HA
			XXX.xx.xx.x|t3.medium|Kubernetes HA Master2|Kubernentes HA
			XXX.xx.xx.x|t3.medium|Kubernetes HA Master3|Kubernentes HA
			XXX.xx.xx.x|t3.medium|Kubernetes HA Worker1|Kubernentes HA
			XXX.xx.xx.x|t3.medium|Kubernetes HA Worker2|Kubernentes HA
			XXX.xx.xx.x|t3.medium|Helm|Kubernentes HA

			Where XXX.xx.xx.x is a available private IP in your app subnet.
			Eg:-
			172.31.16.10|t3.medium|Kubernetes HA Master1|Kubernentes HA
			172.31.16.11|t3.medium|Kubernetes HA Master2|Kubernentes HA
			172.31.16.12|t3.medium|Kubernetes HA Master3|Kubernentes HA
			172.31.16.13|t3.medium|Kubernetes HA Worker1|Kubernentes HA
			172.31.16.14|t3.medium|Kubernetes HA Worker2|Kubernentes HA
			172.31.16.15|t3.medium|Helm|Kubernentes HA

	iii) vi settings.py
		Fill in details for access_id, access_key, key_name, region, subnet_id, avail_zone.
		You can keep the AMI ID as is. Its the official AMI for CentOS 7.
		Fill in subnet_id and security_groups details for your app subnet.

	iv) run python launch_vms.py
		This will provision the 6 instances configured in the kubernetes_ha.txt, required for kubernetes cluster.

	v) edit the kubernetes_ha.txt file, remove all contents and enter the following:-
		ZZZ.zz.zz.z|t3.micro|LoadBalancer|Kubernentes HA
		Where ZZZ.zz.zz.z is a available private IP in your web subnet.
		Eg:-
		172.31.15.121|t3.micro|LoadBalancer|Kubernentes HA
	
	vi) run python launch_vms.py
		This would provision 1 instance in the web subnet required for LoadBalancer.

	vii) cd to kubernetes/Kubernetes directory from the root of the cloned diretory
		edit the hosts file and enter the following:-
		[kubernetes-masters]
		XXX.xx.xx.x pkg="ha-api,containerd, kubernetes" ansible_user=centos host_name=k8s-master1
		XXX.xx.xx.x pkg="containerd, kubernetes" ansible_user=centos	host_name=k8s-master2
		XXX.xx.xx.x pkg="containerd, kubernetes" ansible_user=centos	host_name=k8s-master3
		ZZZ.zz.zz.z  pkg="ha-lb"	ansible_user=centos
		[kubernetes-workers]
		XXX.xx.xx.x pkg="containerd, kubernetes" ansible_user=centos	host_name=k8s-worker1
		XXX.xx.xx.x pkg="containerd, kubernetes" ansible_user=centos	host_name=k8s-worker2
		[helm]
		XXX.xx.xx.x	pkg="helm"	ansible_user=centos	host_name=k8s-helm

		Where XXX.xx.xx.x and ZZZ.zz.zz.z are the private IPs of the instances you provisioned via launch_vms.py

		Eg:-
		[kubernetes-masters]
		172.31.16.10 pkg="ha-api,containerd, kubernetes" ansible_user=centos host_name=k8s-master1
		172.31.16.11 pkg="containerd, kubernetes" ansible_user=centos	host_name=k8s-master2
		172.31.16.12 pkg="containerd, kubernetes" ansible_user=centos	host_name=k8s-master3
		172.31.15.121 pkg="ha-lb"	ansible_user=centos
		[kubernetes-workers]
		172.31.16.13 pkg="containerd, kubernetes" ansible_user=centos	host_name=k8s-worker1
		172.31.16.14 pkg="containerd, kubernetes" ansible_user=centos	host_name=k8s-worker2
		[helm]
		172.31.16.15	pkg="helm"	ansible_user=centos	host_name=k8s-helm

	viii) cd to kubernetes/Kubernetes/kubernetes/files from the root of the cloned diretory
		- Edit haproxy.cfg. At the end of the file replace the IPs in the server block with the IPs of the provisioned instances for kubernetes master.
		- Edit haproxy_ing.cfg. And replace the server IP with the first IP from the [kubernetes-workers] block of the kubernetes host file.
		- Edit kubeadm-config.yaml. Set IP for controlPlaneEndpoint to the first IP from the [kubernetes-masters] block of the kubernetes host file.
		- The LoadBalancer(HAProxy) for kubernetes control plane would be deployed on the first kubernetes master.
		- The haproxy.cfg would be used as configuration for the control plane LoadBalancer.
		- The haproxy_ing.cfg file would be used to route traffic from the external LoadBalancer(HAProxy) in the web subnet(DMZ) to the kubernetes cluster in the app subnet.

### 2) Launch Kubernentes Cluster
	i) Before launching check ssh connectivity from your Ansible master / Bastion host to the provisioned nodes.
	ii) Once verfied launch the cluster with:-
		ansible-playbook -i hosts --private-key ../../provision.pem main.yml
		Replace the provision.pem with your key.
	iii) This will launch :-
		- Kubernentes HA Cluster with 3 masters and 2 nodes
		- A helm server with TLS  and kubectl configured. This would act as our CI server.
		- Calico Ingress as CNI provider

	iv) ssh to the helm server
		- before proceeding further, check if DNS is working fine via:-
			i) kubectl create -f https://k8s.io/examples/admin/dns/busybox.yaml
			ii) kubectl exec -ti busybox -- nslookup kubernetes.default
				If DNS is working correctly, the command returns a response like the following example: 
				Server: 10.96.0.10 
				Address 1: 10.96.0.10 kube-dns.kube-system.svc.cluster.local 
				Name: kubernetes.default 
				Address 1: 10.96.0.1 kubernetes.default.svc.cluster.local

				Errors such as the following message indicate a problem with DNS: 
				Server: 10.0.0.10 
				Address 1: 10.0.0.10
				nslookup: can't resolve 'kubernetes.default'
			iii) If you encounter error, run the below command and check step 2 again:
				kubectl delete pod -n kube-system  -l k8s-app=kube-dns
			iv) Once kube dns is working fine, delete the busy box pod:
				kubectl delete -f https://k8s.io/examples/admin/dns/busybox.yaml

### 3) Deploy guestbook app, Prometheus, Grafana and EFK stack on kubernetes
		i)	ansible-playbook -i hosts --private-key ../../provision.pem deployapps.yaml
			Replace the provision.pem with your key.
			The output of the script would spit out Grafana password. Copy it. Will be used to login in the UI later

		ii) Wait for the pods to come up.
			SSH into the helm server and verfiy all pods are runnning via:-
				i) kubectl get pods -n monitoring
				ii)kubectl get pods -n development
				ii)kubectl get pods -n kube-logging
				iv)kubectl get pods -n ingress-controller
				v)kubectl get pods -n kube-system

		iii) After verfying all the pods are runnning, lets access them via browser.
		Add the below entires to your host file.
			ZZZ.zz.zz.z	guestbook.example.com
			ZZZ.zz.zz.z	grafana.example.com
			ZZZ.zz.zz.z	sampleapp.example.com
			ZZZ.zz.zz.z	sampleapp-staging.example.com
			ZZZ.zz.zz.z	kibana.example.com
	
		Where ZZZ.zz.zz.z is the public IP of the load balancer(HAProxy) configured in the web subnet.

	iv) Access the apps.
		i) Hit http://guestbook.example.com/ in the browser to access Guest book app
		ii) Hit http://grafana.example.com to access Grafana. Enter admin as user name and password copied from step 3(i)
			Once logged in, select the quad-square icon below the plus icon on the left pane. Click on mange
			Click on import. Enter 1860 as a sample dashboard name
			Under the prometheus dropdown select prometheus and select import. This would load the dashboard
			From the top right side of the dashboard, select the time frame as last 5 minutes
			The dashboard would show health of kubernetes nodes
		iii) Hit http://kibana.example.com in the browser
			Use logstash-* as the index pattern

### 4) Blue Green Demo
	A sample app is placed on the helm server for Blue Green Demo. Approach used is to have a staging and prod services and ingress pre-registered on kubernetes, deploy app via helm and route traffic via labels.
	Steps:-
	i) ssh to the helm server
	ii) cd into bluegreen directory
	iii) run "kubectl apply -f service-prod.yml -f service-stag.yml -f ingress-prod.yml -f ingress-stag.yml" to deploy prod and stag services and ingress
	iv) Deploy Blue verion of the app via helm - "helm install --name sampleappv1 ./sampleapp -f config.yaml --tls"
	v) Hit http://sampleapp.example.com/ and http://sampleapp-staging.example.com/. Both would route traffic to the blue version of the app
	vi) edit the config.yaml. change  name to sampleappv2 and appversion to v2.
	vii) Deploy Green version of the app via helm - "helm install --name sampleappv2 ./sampleapp -f config.yaml --tls"
	viii) Edit service-stag.yml and change the selector.app to v2 and run - "kubectl apply -f service-stag.yml"
	ix) This would now route traffic via staging ingress to the green version.
	x) Hit http://sampleapp.example.com/. It would route traffic to blue verion. Hit http://sampleapp-staging.example.com. It would route traffic to green version.
	xi) Test the green version.
	xii) Once testing is complete, Edit service-prod.yml and change the selector.app to v2 and run - "kubectl apply -f service-prod.yml"
	xiii) This would route the production traffic to green version. Verify at http://sampleapp.example.com/
	xiv) Once, verified, delete the blue version via helm - helm delete sampleappv1 --purge --tls
	xv) Hit http://sampleapp.example.com/ and http://sampleapp-staging.example.com/. Both would route traffic to the green version of the app

	- The routing for service and ingress manifest can be further automated with Jenkins and Ansible. Would want to give https://jenkins.io/projects/jenkins-x/ a try.
	- A better solution can be worked upon with Istio.

### 5) Clean Up!
	i) cd into "kubernetes/Kubernetes/provision" directory from root of cloned directory. 
	ii) Edit kubernetes_ha.txt and add all the provisioned IPs
		Eg:-
		172.31.15.121|t3.micro|LoadBalancer|Kubernentes HA
		172.31.16.10|t3.medium|Kubernetes HA Master1|Kubernentes HA
		172.31.16.11|t3.medium|Kubernetes HA Master2|Kubernentes HA
		172.31.16.12|t3.medium|Kubernetes HA Master3|Kubernentes HA
		172.31.16.13|t3.medium|Kubernetes HA Worker1|Kubernentes HA
		172.31.16.14|t3.medium|Kubernetes HA Worker2|Kubernentes HA
		172.31.16.15|t3.medium|Helm|Kubernentes HA

		P.S: Replace 172.* with your provisioned IPs.
	iii) run, python delete_vms.py to delete all provisioned instances.


### Roadmap ahead
- Support multiple distributions(CentOS, RHEL, ubuntu, etc)
- Support multiple cloud solutions(AWS, Google Cloud, Azure, etc)
- Evaluate Jenkins X
- Configure and experiment with Istio
- Configure dynamic provisioning for storage
- Harden kubernetes cluster with respect to security parameters
- Test kubernetes cluster for security with tools like kube-bench
- Test kubernetes cluster completenes with https://sonobuoy.io/
- Automate the script further to remove any manual touch points
- Better documentation! :D
			



